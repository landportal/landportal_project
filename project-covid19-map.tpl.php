<?php
$data = [];
foreach ($rows as $key => $value) {
 //$data[] = array($key, $value['total_counter'] . $value['content_types'] . $value['newest']);
 $data[] = array($key, "<br />" . $value['content_types'] . $value['newest']);
}
?>


<script src="https://code.highcharts.com/maps/highmaps.js"></script>
<script src="https://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="https://code.highcharts.com/mapdata/custom/world.js"></script>

<div id="container-map"></div>


<script type="text/javascript">
// Prepare demo data
// Data is joined to map using value of 'hc-key' property by default.
// See API docs for 'joinBy' for more info on linking data and map.

var data = <?php print drupal_json_encode($data);  ?>

// Create the chart
Highcharts.mapChart('container-map', {
    chart: {
        map: 'custom/world'
    },

    title: {
        text: ' Content items by country for COVID19'
    },

    subtitle: {
        text: 'Source map: <a href="http://code.highcharts.com/mapdata/custom/world.js">World, Miller projection, medium resolution</a>'
    },

    mapNavigation: {
        enabled: true,
        buttonOptions: {
            verticalAlign: 'bottom'
        }
    },

    colorAxis: {
        min: 0
    },

    series: [{
        data: data,
        name: 'Content items by country',
        states: {
            hover: {
                color: '#BADA55'
            }
        },
        dataLabels: {
            enabled: true,
            format: '{point.name}'
        }
    }]
});


</script>
