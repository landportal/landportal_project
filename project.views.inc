<?php
/**
 * Implementation of hook_views_query_alter
 * @param type $view
 * @param type $query 
 */
function project_views_query_alter(&$view, &$query) {

  // for implementors field
  if ($view->name == 'views_field_formatter_for_entity_content_2') {

    unset($view->query->orderby); 
    $orderByCond =  "FIELD(node.nid, " . $view->args[0] .  ")";
    $orderByCond = array('field' => $orderByCond);
    $view->query->orderby[] = $orderByCond;

  }
}

