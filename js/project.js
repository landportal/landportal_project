(function ($) {
  Drupal.behaviors.project2 = {
    attach: function (context, settings) {

  $(".node-type-project .group-details .field-name-field-currency .field-items .field-item").each(function(){
      var arr = {"United States dollar":"$", "Canadian dollar":"$", "Euro":"€", "Pound sterling":"£", "Swiss franc":"₣"};
      var currency = $(this).html();
      if (typeof currency !== 'undefined') {
        $(this).text(arr[currency]);
      }
      else {
        $(this).text("");
      }
    });

}
}

})(jQuery);

