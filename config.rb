require 'compass/import-once/activate'
require 'bootstrap-sass'
require 'font-awesome-sass'

###########################################
#
#       Compiler configuration
#
Encoding.default_external = "utf-8"
# Path configuration
css_dir = "css/"
sass_dir = "scss/"
images_dir = "images/"
relative_assets = true

# don't touch this
preferred_syntax = :scss

sass_options = {:sourcemap => :inline}

###########################################
#
#       Environments
#

# DEV
if (environment.nil?)
  environment = :production
end
sourcemap = (environment == :production) ? false : true
output_style = (environment == :production) ? :compact : :expanded

