(function ($) {
  Drupal.behaviors.project = {
    attach: function (context, settings) {

    if ($(".view-Projects .views-exposed-widgets-inner-1", context).length == 0) {
        $(".view-Projects .views-exposed-widgets", context).append("<div class='views-exposed-widgets-inner-1'></div>");
        $(".view-Projects .views-exposed-widgets-inner-1", context).addClass("clearfix");
        $(".view-Projects .views-exposed-widgets-inner-1", context).append($(".views-widget-filter-title"));
        $(".view-Projects .views-exposed-widgets-inner-1", context).append($(".views-widget-filter-field_geographical_focus_tid"));
        $(".view-Projects .views-exposed-widgets-inner-1", context).append($(".views-widget-filter-field_related_themes_tid"));
        $(".view-Projects .views-exposed-widgets-inner-1 .views-widget-filter-title", context).addClass("col-md-4");
        $(".view-Projects .views-exposed-widgets-inner-1 .views-widget-filter-field_geographical_focus_tid", context).addClass("col-md-4");
        $(".view-Projects .views-exposed-widgets-inner-1 .views-widget-filter-field_related_themes_tid", context).addClass("col-md-4");
    }
    if ($(".view-Projects .views-exposed-widgets-inner-2", context).length == 0) {
        $(".view-Projects .views-exposed-widgets", context).append("<div class='views-exposed-widgets-inner-2'></div>");
        $(".view-Projects .views-exposed-widgets-inner-2", context).addClass("clearfix");
        $(".view-Projects .views-exposed-widgets-inner-2", context).append($(".views-widget-filter-field_donors_target_id_entityreference_filter"));
        $(".view-Projects .views-exposed-widgets-inner-2", context).append($(".views-widget-filter-field_implementers_target_id_entityreference_filter"));
        $(".view-Projects .views-exposed-widgets-inner-2", context).append($(".views-widget-filter-field_date_value"));
        $(".view-Projects .views-exposed-widgets-inner-2", context).append($(".views-widget-filter-field_date_value2"));
        $(".view-Projects .views-exposed-widgets-inner-2", context).append($(".views-widget-filter-field_status_by_date_value"));
        $(".view-Projects .views-exposed-widgets-inner-2", context).append($(".views-submit-button"));
        $(".view-Projects .views-exposed-widgets-inner-2 .views-widget-filter-field_donors_target_id_entityreference_filter", context).addClass("col-md-3");
        $(".view-Projects .views-exposed-widgets-inner-2 .views-widget-filter-field_implementers_target_id_entityreference_filter", context).addClass("col-md-3");
        $(".view-Projects .views-exposed-widgets-inner-2 .views-widget-filter-field_date_value", context).addClass("col-md-3");
        $(".view-Projects .views-exposed-widgets-inner-2 .views-widget-filter-field_date_value2", context).addClass("col-md-3");
        $(".view-Projects .views-exposed-widgets-inner-2 .views-widget-filter-field_status_by_date_value", context).addClass("col-md-3");
        $(".view-Projects .views-exposed-widgets-inner-2 .views-submit-button", context).addClass("col-md-1");
    }
    if ($(".view-Projects .views-exposed-widgets-inner-3", context).length == 0) {
        $(".view-Projects .views-exposed-widgets", context).append("<div class='views-exposed-widgets-inner-3'></div>");
        $(".view-Projects .views-exposed-widgets-inner-3", context).addClass("clearfix");
        $(".view-Projects .views-exposed-widgets-inner-3", context).append($(".view-Projects.view-display-id-block_5 .view-header"));
    }
    if ($(".view-Projects .views-exposed-widgets-inner-4", context).length == 0) {
        $(".view-Projects .views-exposed-widgets", context).append("<div class='views-exposed-widgets-inner-4'></div>");
        $(".view-Projects .views-exposed-widgets-inner-4", context).addClass("clearfix");
        $(".view-Projects .views-exposed-widgets-inner-4", context).append($(".views-widget-sort-by"));
        $(".view-Projects .views-exposed-widgets-inner-4", context).append($(".views-widget-sort-order"));
    }

  $(".view-Projects .node-project.node-teaser .field-name-field-date").each(function(){
      $("<div class='field-group-currency clearfix'></div>").insertAfter(this);
    });
  $(".view-Projects .node-project.node-teaser").each(function(){
      $(this).find(".field-group-currency").append($(this).find(".field-name-field-value-decimal"));
      $(this).find(".field-group-currency").append($(this).find(".field-name-field-currency"));
    });

  $(".view-Projects .node-project.node-teaser .field-name-field-currency .field-item").each(function(){
      var value =  jQuery(this).parents(".node-teaser").find(".field-name-field-value-decimal").find(".field-item").html();
      var arr = {"United States dollar":"$", "Canadian dollar":"$", "Euro":"€", "Pound sterling":"£", "Swiss franc":"₣"};
      var currency = $(this).html();
      if (typeof value !== 'undefined') {
        $(this).text(arr[currency]);
      }
      else {
        $(this).text("");
      }
    });



  $(".view-Projects .node-project.node-teaser .field-name-body .field-items").each(function(){
      var text = $(this).text();
      $(this).text(text + " ...");
    });

   }
  }
})(jQuery);



