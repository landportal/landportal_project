<?php

//dpm($rows);
 $data = [];
 foreach ($rows as $row) {
   $item['name'] = $row['title'];
   $item['y'] = $row['sum'];
   $item['p'] = $row['percentage'];
   $data[] = $item;
}
//dpm($data);

?>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>

<figure class="highcharts-figure">
    <div id="container-donut"></div>
    <p class="highcharts-description"></p>
</figure>


<script type="text/javascript">
    // save original
    var origHC = window.Highcharts;
    delete window.Highcharts;

// Create the chart
origHC.setOptions({lang: {thousandsSep: ','}});
origHC.chart('container-donut', {
    chart: {
        type: 'pie'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },

    accessibility: {
        announceNewData: {
            enabled: true
        },
        point: {
            valueSuffix: 'USD'
        }
    },

    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.name}: {point.y:,.2f} USD'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.p:.2f}%</b> of total<br/>'
    },

    series: [
        {
            name: "Donors",
            colorByPoint: true,
            data: <?php print drupal_json_encode($data); ?> 
        }
    ]
});

</script>




