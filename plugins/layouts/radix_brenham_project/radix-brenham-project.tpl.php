<?php
/**
 * @file
 * Template for Radix Brenham.
 *
 * Variables:
 * - $css_id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 * panel of the layout. This layout supports the following sections:
 */
?>

<div class="panel-display brenham clearfix <?php if (!empty($classes)) { print $classes; } ?><?php if (!empty($class)) { print $class; } ?>" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 radix-layouts-header panel-panel">
        <div class="panel-panel-inner">
          <?php print $content['header']; ?>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-4 radix-layouts-sidebar panel-panel landportal-project-left">

        <div class="panel-panel-inner">
          <?php print $content['sidebar_0']; ?>
        </div>

        <div class="panel-panel-inner">
          <div class="pane-title"><h2> Project Information </h2></div>
          <?php print $content['sidebar_1']; ?>
        </div>

        <div class="panel-panel-inner">

          <div class="pane-title"><h2> Who is involved? </h2></div>
          <?php print $content['sidebar_3']; ?>
        </div>

        <div class="panel-panel-inner">
          <?php print $content['sidebar_4']; ?>
        </div>


      </div>
      <div class="col-md-8 radix-layouts-content panel-panel landportal-project-right">
        <div class="panel-panel-inner content-main-1">
          <?php print $content['contentmain_1']; ?>
        </div>
        <div class="panel-panel-inner content-main-2">
<?php if (!empty($content['contentmain_2'])): ?>
  <div class="pane-title"><h2>Project outcomes and documentation</h2></div>
  <?php print $content['contentmain_2']; ?>
<?php endif; ?>
        </div>

        <div class="panel-panel-inner content-main-3">
<?php if (!empty($content['contentmain_3'])): ?>
  <div class="pane-title"><h2>Themes</h2></div>
  <?php print $content['contentmain_3']; ?>
<?php endif; ?>
        </div>


        <div class="panel-panel-inner content-main-4">
<?php if (!empty($content['contentmain_4'])): ?>
  <div class="pane-title"><h2> Topics</h2></div>
  <?php print $content['contentmain_4']; ?>
<?php endif; ?>
        </div>





      </div>
    </div>
  </div>

</div><!-- /.brenham -->

