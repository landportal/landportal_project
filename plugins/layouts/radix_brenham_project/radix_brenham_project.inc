<?php
// Plugin definition
$plugin = array(
  'title' => t('Brenham Project'),
  'icon' => 'radix-brenham-project.png',
  'category' => t('Landportal'),
  'theme' => 'radix_brenham_project',
  'regions' => array(
    'header' => t('Header'),
    'sidebar_0' => t('Content Sidebar 0'),
    'sidebar_1' => t('Content Sidebar 1'),
    'sidebar_3' => t('Content Sidebar 3'),
    'sidebar_4' => t('Content Sidebar 4'),
    'contentmain_1' => t('Content 1'),
    'contentmain_2' => t('Content 2'),
    'contentmain_3' => t('Content 3'),
    'contentmain_4' => t('Content 4'),
  ),
);
