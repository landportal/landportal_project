<?php
/**
 * @file
 * Template for Radix Brenham.
 *
 * Variables:
 * - $css_id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 * panel of the layout. This layout supports the following sections:
 */
?>

<div class="panel-display silvina clearfix <?php if (!empty($classes)) { print $classes; } ?><?php if (!empty($class)) { print $class; } ?>" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>

  <div class="container-fluid">

    <div class="row">
      <div class="col-md-4 radix-layouts-sidebar panel-panel community-project-left">

        <div class="panel-panel-inner">
          <?php print $content['sidebar']; ?>
        </div>

      </div>
      <div class="col-md-8 radix-layouts-content panel-panel community-project-right">

        <div class="panel-panel-inner content-main-1 col-md-12">
          <?php print $content['contentmain_1']; ?>
        </div>

        <div class="panel-panel-inner content-main-2 col-md-12">

            <div class="col-md-6 panel-panel-inner content-main-2-1 no-pad">
              <?php print $content['contentmain_2_1']; ?>
            </div>

            <div class="col-md-6 panel-panel-inner content-main-2-2 no-pad">
              <?php print $content['contentmain_2_2']; ?>
            </div>

        </div>
        <div class="panel-panel-inner content-main-3 col-md-12">
          <?php print $content['contentmain_3']; ?>
        </div>
       </div>
    </div>
  </div>

</div><!-- /.silvina -->

