<?php
// Plugin definition
$plugin = array(
  'title' => t('Silvina Project'),
  'icon' => 'radix-silvina-project.png',
  'category' => t('Landportal'),
  'theme' => 'radix_silvina_project',
  'regions' => array(
    'sidebar' => t('Content Sidebar'),
    'contentmain_1' => t('Content 1'),
    'contentmain_2_1' => t('Content 2'),
    'contentmain_2_2' => t('Content 3'),
    'contentmain_3' => t('Content 4'),
  ),
);
