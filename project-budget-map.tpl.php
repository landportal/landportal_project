<?php
$rows = array_values($rows);
$data = [];
foreach ($rows as $row) {
 //$data[] = array($row['country'], 'budget: ' . number_format($row['budget']) . ' USD, number of projects: ' . $row['projects']);
 $data[] = array($row['country'], 'number of projects: ' . $row['projects']);
}
?>


<script src="https://code.highcharts.com/maps/highmaps.js"></script>
<script src="https://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="https://code.highcharts.com/mapdata/custom/world.js"></script>

<div id="container-map"></div>


<script type="text/javascript">
// Prepare demo data
// Data is joined to map using value of 'hc-key' property by default.
// See API docs for 'joinBy' for more info on linking data and map.

var data = <?php print drupal_json_encode($data);  ?>

// Create the chart
Highcharts.mapChart('container-map', {
    chart: {
        map: 'custom/world'
    },

    title: {
        text: 'Projects by country'
    },

    subtitle: {
        text: 'Source map: <a href="http://code.highcharts.com/mapdata/custom/world.js">World, Miller projection, medium resolution</a>'
    },

    mapNavigation: {
        enabled: true,
        buttonOptions: {
            verticalAlign: 'bottom'
        }
    },

    colorAxis: {
        min: 0
    },

    series: [{
        data: data,
        name: 'Projects by country',
        states: {
            hover: {
                color: '#BADA55'
            }
        },
        dataLabels: {
            enabled: true,
            format: '{point.name}'
        }
    }]
});


</script>
